package com.flyfox.component.controller;

import com.flyfox.jfinal.component.annotation.ControllerBind;
import com.flyfox.jfinal.component.umeditor.UmeditorController;

@ControllerBind(controllerKey = "umeditor")
public class Umeditor extends UmeditorController {

}
